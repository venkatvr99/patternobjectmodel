package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeads extends ProjectMethods {

	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="//span[contains(text(),'Email')]")
	WebElement eleClickEmailTab;
	
	@FindBy(how=How.NAME,using="emailAddress")
	WebElement eleEnterEmailID;
	
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']")
	WebElement eleClickFindLeadsBtn;
	
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[6]")
	WebElement eleFirstNameInResult;
	
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[7]")
	WebElement eleLastNameInResult;
	
	@FindBy(how=How.XPATH,using="(//*[@name='id']//following::input)[1]")
	WebElement eleFirstNameField;
	
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[4]")
	WebElement eleClickFirstLead;
	
		
	public FindLeads clickEmailTab() {
		click(eleClickEmailTab);
		return this;
	}
	public FindLeads enterEmailID(String data) {
		clearAndType(eleEnterEmailID, data);
		return this;
	}
	
	public FindLeads clickFindLeadsBtn() throws InterruptedException {
		click(eleClickFindLeadsBtn);
		Thread.sleep(3000);
		return this;
	}
	
	public FindLeads captureFirstNameinResult() throws InterruptedException {
		getElementText(eleFirstNameInResult);
		Thread.sleep(2000);
		return this;
	}
	public FindLeads captureLastNameinResult() throws InterruptedException {
		getElementText(eleLastNameInResult);
		Thread.sleep(2000);
		return this;
	}
	public ViewLeadsPage clickOnFirstLeadingResult() {
		clickWithNoSnap(eleFirstNameInResult);
		return new ViewLeadsPage();
	}
	
	public FindLeads enterFirstName(String data) {
		clearAndType(eleFirstNameField, data);
		return this;
	}
	
}
