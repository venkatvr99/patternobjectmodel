package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="updateLeadForm_companyName")
	WebElement eleCompanyName;
	
	@FindBy(how=How.NAME,using="submitButton")
	WebElement eleClickUpdateBtn;
	
	
	public EditLeadPage updateCompanyName(String data) {
		clearAndType(eleCompanyName, data);
		return this;
	}
	public ViewLeadsPage clickUpdateButton() {
		clickWithNoSnap(eleClickUpdateBtn);
		return new ViewLeadsPage();
	}
}
