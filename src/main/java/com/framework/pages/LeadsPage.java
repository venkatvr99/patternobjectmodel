package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadsPage extends ProjectMethods {

	public LeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead")
	WebElement eleClickCreateLead;
	
	@FindBy(how=How.LINK_TEXT,using="Find Leads")
	WebElement eleClickFindLeads;
	
	public CreateLeadPage clickCreateLead() {
		clickWithNoSnap(eleClickCreateLead);
		return new CreateLeadPage();
	}
	public FindLeads clickFindLeads() {
		clickWithNoSnap(eleClickFindLeads);
		return new FindLeads();
		
	}
}
