package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC004_MergeLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC004_MergeLead";
		testDescription="Merge two leads";
		author="Venkat";
		category="Integration Test";
		dataSheetName="TC004";
		testNodes="Leads";
	}
	@Test
	public void mergeLead() {
		new LoginPage()
		.enterUsername("")
		.enterPassword("")
		.clickLogin()
		.clickCRMSFALink()
		.clickLeads();		
	}
}
