package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testDescription="Create a new Lead in leaftaps";
		author="Venkat";
		testNodes="Leads";
		category="functional";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void createLead(String userName,String password,String companyName,String firstName,String lastName) {
		//LoginPage lp=new LoginPage();
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFALink()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.eleClickCreateLeadButton()
		.verifyFirstName(firstName);
	}
}
