package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="Edit the created lead";
		author="Venkat";
		category="Integration Test";
		dataSheetName="TC003";
		testNodes="Leads";
	}

	@Test(dataProvider="fetchData")
	public void editLead(String userName,String password,String firstName,String title,String companyName) throws InterruptedException {
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFALink()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(firstName)
		.clickFindLeadsBtn()
		.clickOnFirstLeadingResult()
		.verifyTitleAfterFindLead(title)
		.clickEditButton()
		.updateCompanyName(companyName)
		.clickUpdateButton()
		.verifyUpdatedCompanyName(companyName);
	}
}
