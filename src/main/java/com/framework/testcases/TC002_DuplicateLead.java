package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_DuplicateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC002_DuplicateLead";
		testDescription="Creates a duplicate lead";
		author="Venkat";
		category="Integration Test";
		dataSheetName="TC002";
		testNodes="Leads";
	}
	@Test(dataProvider="fetchData")
	public void duplicateLead(String userName,String password,String emailID,String title,String name) throws InterruptedException {
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFALink()
		.clickLeads()
		.clickFindLeads()
		.clickEmailTab()
		.enterEmailID(emailID)
		.clickFindLeadsBtn()
		.captureFirstNameinResult()
		.captureLastNameinResult()
		.clickOnFirstLeadingResult()
		.clickDuplicateLead()
		.verifyTitleAsDuplicateLead(title)
		.clickCreateLeadInDupLead()
		.verifyFirstName(name);
	}
}
